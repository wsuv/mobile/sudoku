//
//  Sudoku.swift
//  sudoku
//
//  Created by John Karasev on 3/26/19.
//  Copyright © 2019 John Karasev. All rights reserved.
//

import Foundation
import UIKit


enum PuzzleType {
    case simple
    case hard
}


//structure for cell, then two demensional array of cells.
struct CellInfo {
    
    var isFixed : Bool
    var number: Int
    var subBoxCoordinates: (x: Int, y: Int)
    var numberCoordinates: (row: Int, col: Int)
    var pencilNumbers: [Int]
    
    init(row: Int, col: Int, number: Int) {
        if number == 0 {
            self.isFixed = false
        } else {
            self.isFixed = true
        }
        self.number = number
        self.numberCoordinates = (row: row, col: col)
        self.subBoxCoordinates = (x: row / 3, y: col / 3)
        self.pencilNumbers = []
    }
}

class SudokuModel {
    
    var board: [[CellInfo]];
    
    lazy var simplePuzzles = getPuzzles("simple")
    lazy var hardPuzzles = getPuzzles("hard")
    
    init(board: String) {
        self.board = [[CellInfo]]()
        var strArray = Array(board)
        for i in 0..<9 {
            var row = [CellInfo]()
            for j in 0..<9 {
                var number: Int;
                if strArray[i+j*9] == "." {
                    number = 0; // if empty store zero
                } else {
                    number = Int(String(strArray[i+j*9]))!
                }
                row.append(CellInfo(row: i, col: j, number: number))
            }
            self.board.append(row)
        }
    }
    
    init() {
        self.board = [[CellInfo]]()
    }

    func createPuzzle(type: PuzzleType) {
        self.board = [[CellInfo]]()
        if type == .hard {
            populateBoard(board: self.hardPuzzles.randomElement()!)
        } else {
            populateBoard(board: self.simplePuzzles.randomElement()!)
        }
    }
    
    func getPuzzles(_ name: String) -> [String] {
        guard let url = Bundle.main.url(forResource: name, withExtension: "plist")
            else { return [] }
        guard let data = try? Data(contentsOf: url) else {return [] }
        guard let array = try? PropertyListDecoder().decode([String].self, from: data)
            else { return [] }
        return array
    }
    
    func populateBoard(board: String) {
        var strArray = Array(board)
        for i in 0..<9 {
            var row = [CellInfo]()
            for j in 0..<9 {
                var number: Int;
                if strArray[i+j*9] == "." {
                    number = 0;
                } else {
                    number = Int(String(strArray[i+j*9]))!
                }
                row.append(CellInfo(row: i, col: j, number: number))
            }
            self.board.append(row)
        }
    }
    
    func numberAt(row : Int, column : Int) -> Int {
        if board[row][column].pencilNumbers.count > 0 {
            return 0
        } else {
           return board[row][column].number
        }
    }
    
    func numberIsFixedAt(row : Int, column : Int) -> Bool {
        return board[row][column].isFixed
    }
    
    func isConflictingEntryAt(row : Int, column: Int) -> Bool {
        
        if board[row][column].isFixed {
            return false
        }
        
        let flatBoard = Array(board.joined())
        
        let subBoxCoordinates = (x: row / 3, y: column / 3)
        
        let checkingNumber = board[row][column].number
        
        // check 3x3 subbox
        let subBoxCount = flatBoard.filter({ (cell: CellInfo) in
            cell.subBoxCoordinates.x == subBoxCoordinates.x &&
                cell.subBoxCoordinates.y == subBoxCoordinates.y
        }).filter({ (cell: CellInfo) in
            cell.number == checkingNumber
        }).count
        
        // check row
        let rowNumbersCount = flatBoard.filter({ (cell: CellInfo) in
            cell.numberCoordinates.row == row
        }).filter({ (cell: CellInfo) in
            cell.number == checkingNumber
        }).count
        
        // check column
        let colNumbersCount = flatBoard.filter({ (cell: CellInfo) in
            cell.numberCoordinates.col == column
        }).filter({ (cell: CellInfo) in
            cell.number == checkingNumber
        }).count
        
        
        if subBoxCount > 1 || rowNumbersCount > 1 || colNumbersCount > 1 {
            return true
        } else {
            return false
        }
        
    }
    
    
    func anyPencilSetAt(row : Int, column : Int) -> Bool {
        return board[row][column].pencilNumbers.count > 0
    }
    
    
    func isSetPencil(_ n : Int, row : Int, column : Int) -> Bool {
        return board[row][column].pencilNumbers.contains(n)
    }
    
    
    func togglePencilNumber(row: Int, column: Int, number: Int) {
       
        if board[row][column].isFixed {
            return
        }
        
        //cannot overwrite numbers
        if board[row][column].number != 0 {
            return
        }
        
        if board[row][column].pencilNumbers.contains(number) {
            board[row][column].pencilNumbers.removeAll(where: {$0 == number})
        } else  {
           board[row][column].pencilNumbers.append(number)
        }
    }
    
    
    func setNumber(row: Int, column: Int, number: Int) {
        
        if board[row][column].isFixed {
            return
        }
        
        // cannot overwrite pencil marks.
        if board[row][column].pencilNumbers.count > 0 {
            return
        }
        
        // number is cleared if the same one is tapped.
        if board[row][column].number == number {
            board[row][column].number = 0
            return
        }
        
        // User cannot overwrite a number unless clearing the cell (i.e. number = 0)
        if board[row][column].number != 0 && number != 0 {
            return
        }
        
        board[row][column].number = number
        
    }
    
    func clearCell(row: Int, column: Int) {
        setNumber(row: row, column: column, number: 0)
    }
    
    func clearAllPencilMarks(row: Int, column: Int) {
        
        if board[row][column].isFixed {
            return
        }
        
        board[row][column].pencilNumbers = []
        
    }
    
    
    func clearConflicts() {
        for row in 0..<board.count {
            for col in 0..<board[0].count {
                if isConflictingEntryAt(row: row, column: col) &&
                    !board[row][col].isFixed {
                    board[row][col].number = 0
                }
            }
        }
    }
    
    
    func clearPencilNumbers() {
        for row in 0..<board.count {
            for col in 0..<board[0].count {
                if !board[row][col].isFixed {
                    board[row][col].pencilNumbers = []
                }
            }
        }
    }
    
    func clearEverything() {
        for row in 0..<board.count {
            for col in 0..<board[0].count {
                if !board[row][col].isFixed {
                    board[row][col].pencilNumbers = []
                    board[row][col].number = 0
                }
            }
        }
    }
    
    func isEmpty() -> Bool {
        if board.count == 0 {
            return true
        } else {
            return false
        }
    }
    
    func deleteBoard() {
        self.board = []
    }
}
