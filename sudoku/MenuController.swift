//
//  MenuController.swift
//  sudoku
//
//  Created by John Karasev on 4/2/19.
//  Copyright © 2019 John Karasev. All rights reserved.
//

import Foundation
import UIKit

class MenuController: UIViewController {
    
    @IBOutlet weak var resumeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated:true);
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if appDelegate.sudoku.isEmpty()  {
            resumeButton.isHidden = true
        } else {
            resumeButton.isHidden = false
        }
    }
    
    @IBAction func startNewGame(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let gameOptions = sb.instantiateViewController(withIdentifier: "gameOptions")
        if appDelegate.sudoku.board.count > 0 {
            let alert = UIAlertController(title: "Discard Saved Puzzle?", message: "", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "yes", style: .destructive) {(action: UIAlertAction) in
                appDelegate.sudoku.deleteBoard()
                self.navigationController?.pushViewController(gameOptions, animated: true)
            })
            alert.addAction(UIAlertAction(title: "cancel", style: .cancel) {(action: UIAlertAction) in
                return
            })
            self.present(alert, animated: true)
        } else {
            self.navigationController?.pushViewController(gameOptions, animated: true)
        }
    }
    
    @IBAction func unwindToMain(segue: UIStoryboardSegue) {}
    
}
