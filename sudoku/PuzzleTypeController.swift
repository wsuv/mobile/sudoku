//
//  PuzzleTypeController.swift
//  sudoku
//
//  Created by John Karasev on 4/2/19.
//  Copyright © 2019 John Karasev. All rights reserved.
//

import Foundation
import UIKit


class PuzzleTypeController: UIViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    @IBAction func createSimple(_ sender: UIButton) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.sudoku.createPuzzle(type: .simple)
        
    }
    
    @IBAction func createDifficult(_ sender: UIButton) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.sudoku.createPuzzle(type: .hard)
    }
    
    
}
