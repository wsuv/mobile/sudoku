//
//  ViewController.swift
//  sudoku
//
//  Created by John Karasev on 3/26/19.
//  Copyright © 2019 John Karasev. All rights reserved.
//

import UIKit

class SudokuController: UIViewController {
    
    //@IBOutlet weak var sudokuView: SudokuView!
    
    @IBOutlet weak var sudokuView: SudokuView!
    //let sview: SudokuView = SudokuView()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    
    @IBAction func insertNumber(_ button: UIButton) {
        
        if sudokuView.selected.row == -1 || sudokuView.selected.column == -1 {
            return
        }
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        if sudokuView.isPencil {
            appDelegate.sudoku.togglePencilNumber(row: sudokuView.selected.row, column: sudokuView.selected.column, number: button.tag)
        } else {
            appDelegate.sudoku.setNumber(row: sudokuView.selected.row, column: sudokuView.selected.column, number: button.tag)
        }
        
        sudokuView.setNeedsDisplay()
        
    }
    @IBAction func leavePuzzle(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let alert = UIAlertController(title: "Discard Puzzle?", message: "", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "cancel", style: .cancel, handler: nil))
        
        alert.addAction(UIAlertAction(title: "yes", style: .destructive, handler: {(res: UIAlertAction) in
            //destroy the board
            appDelegate.sudoku.deleteBoard()
            //let sb = UIStoryboard(name: "Main", bundle: nil)
            self.performSegue(withIdentifier: "unwindToMain", sender: self)
        }))
        
        self.present(alert, animated: true)
        
        
    }
    
    @IBAction func clearNumber(_ button: UIButton) {
        
        if sudokuView.selected.row == -1 || sudokuView.selected.column == -1 {
            return
        }
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        if !sudokuView.isPencil {
            //appDelegate.sudoku.setNumber(row: sudokuView.selected.row, column: sudokuView.selected.column, number: 0)
            appDelegate.sudoku.clearCell(row: sudokuView.selected.row, column: sudokuView.selected.column)
        } else {
            appDelegate.sudoku.clearAllPencilMarks(row: sudokuView.selected.row, column: sudokuView.selected.column)
        }
        
        sudokuView.setNeedsDisplay()
        
    }
    
    @IBAction func pencilToggle(_ button: UIButton) {
        if sudokuView.isPencil {
            button.backgroundColor = UIColor.white
            sudokuView.isPencil = false
        } else {
            button.backgroundColor = UIColor.gray
            sudokuView.isPencil = true
        }
    }
    
    
    @IBAction func showMenu(_ sender: UIButton) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let alert = UIAlertController(title: "Menu", message: "", preferredStyle: .alert)
        
        
        alert.addAction(UIAlertAction(title: "clear conflicts", style: .destructive, handler: {(res: UIAlertAction) in
                appDelegate.sudoku.clearConflicts()
                self.sudokuView.setNeedsDisplay()
            }))
        
        alert.addAction(UIAlertAction(title: "clear pencil marks", style: .destructive, handler: {(res: UIAlertAction) in
            appDelegate.sudoku.clearPencilNumbers()
            self.sudokuView.setNeedsDisplay()
        }))
        
        alert.addAction(UIAlertAction(title: "clear everything", style: .destructive, handler: {(res: UIAlertAction) in
            self.sudokuView.selected = (row: -1, column: -1)
            appDelegate.sudoku.clearEverything()
            self.sudokuView.setNeedsDisplay()
        }))
        
        alert.addAction(UIAlertAction(title: "cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true)
    }
    
}

